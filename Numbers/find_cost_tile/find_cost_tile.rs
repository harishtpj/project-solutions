/* Rust program to Calculate the total cost of tile it would take to cover
 * a floor plan of width and height, using a cost entered by the user
 *
 * Author     : M.V.Harish Kumar
 * Created on : 03-05-2023
 */

use std::io::{self, Write};

fn prompt(var: &mut String, prompt: &str) {
    print!("{}", prompt);
    io::stdout().flush().unwrap();

    io::stdin().read_line(var).expect("Failed to read line");
}

fn main() {
    let mut ibuf = String::new();

    prompt(&mut ibuf, "Enter floor width: ");
    let width: f32 = ibuf.trim().parse().expect("Invalid Number");

    ibuf.clear();
    prompt(&mut ibuf, "Enter floor length: ");
    let length: f32 = ibuf.trim().parse().expect("Invalid Number");

    ibuf.clear();
    prompt(&mut ibuf, "Enter tile length: ");
    let tile_len: f32 = ibuf.trim().parse().expect("Invalid Number");

    ibuf.clear();
    prompt(&mut ibuf, "Enter cost per tile: ");
    let tile_cost: f32 = ibuf.trim().parse().expect("Invalid Number");

    let cost: f32 = calc_cost(length * width, tile_cost, tile_len * tile_len);
    println!("Total cost is: {cost}");
}

fn calc_cost(floor_area: f32, tile_cost: f32, tile_area: f32) -> f32 {
    let mut no_of_tiles: i32 = (floor_area / tile_area) as i32;
    if (floor_area as i32) % (tile_area as i32) != 0 {
        no_of_tiles += 1;
    }
    (no_of_tiles as f32) * tile_cost
}
