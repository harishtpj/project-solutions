/* Rust program to generate the nth Fibnocci Number
 * or generate fibnocci numbers till nth number.
 *
 * Author     : M.V.Harish Kumar
 * Created on : 03-05-2023
 */

use std::io::{self, Write};

fn fib(n: u8) -> u32 {
    if n <= 1 {
        return n.into();
    }
    fib(n - 2) + fib(n - 1)
}

fn print_nth_no() {
    let mut n = String::new();

    print!("Enter value for 'n': ");
    io::stdout().flush().unwrap();

    io::stdin().read_line(&mut n).expect("Failed to read line");

    let n: u8 = n
        .trim()
        .parse()
        .expect("Please enter an valid input of range [1, 255]");

    println!("The {n}th fibnocci number is {}", fib(n));
}

fn gen_seq() {
    let mut n = String::new();

    print!("Enter value for 'n': ");
    io::stdout().flush().unwrap();

    io::stdin().read_line(&mut n).expect("Failed to read line");

    let n: u8 = n
        .trim()
        .parse()
        .expect("Please enter an valid input of range [1, 255]");

    let mut seq: Vec<u32> = vec![];

    for i in 1..=n {
        seq.push(fib(i));
    }

    let seq = seq
        .iter()
        .map(|no| no.to_string())
        .collect::<Vec<String>>()
        .join(", ");

    println!("The Fibnocci sequence till {n} is: {seq}");
}

fn main() {
    let mut opt = String::new();
    println!("Fibnocci number generator");

    loop {
        opt.clear();
        println!("[1]. Print nth number");
        println!("[2]. Generate Sequence");
        println!("[3]. Quit Program");

        print!("Enter your option: ");
        io::stdout().flush().unwrap();

        io::stdin()
            .read_line(&mut opt)
            .expect("Failed to read line");

        let opt: u8 = opt.trim().parse().expect("Please enter an valid input");

        println!(" ");
        match opt {
            1 => {
                print_nth_no();
                break;
            }
            2 => {
                gen_seq();
                break;
            }
            3 => break,
            _ => {
                eprintln!("Error: Invalid Input");
                continue;
            }
        }
    }

    println!("Program Exited");
}
